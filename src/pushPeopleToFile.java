﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter; 
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.logging.Level; 
import java.util.logging.Logger; 

/** * Represents a task that inserts a number of people in a text file. * 
 * @author Eugenia Pérez Martínez */ 
class PushPeopleToFile implements Runnable { 
    
    /** * Number of people to insert. */ 
    private int numberOfPeople; 
    
    /** * Class constructor. * 
     * @param numberOfPeople people to insert. */ 
    public PushPeopleToFile (int numberOfPeople) { 
        this.numberOfPeople = numberOfPeople;
    } 

    @Override 
    public void run() { 
        System.out.println("Starting to push people to a text file");
        //TODO: Crea los objetos necesarios para escribir a fichero en Java. 
        
        
        String fname = "personas.txt";
        File fichero = new File(fname);
         
        //if (fichero.exists()) {
       
       //StringWriter write = new StringWriter();
       BufferedWriter writer = null;
	try {
		writer = new BufferedWriter(new FileWriter(fichero));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
		for (int x=0;x<numberOfPeople;x++){
			
				Persona per = new Persona();
	        	System.out.println((x+1) + "ª  " +per.toString()+ " FICHERO");
	        	
				try {
					
					//writer.write("PERSONA : " + x+1 + " DNI: "+ per.dni + " EDAD: " + per.age +"\n");
					writer.write((x+1) + "ª  " + per.toString()+ "\n");
					writer.newLine();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			
				
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		
		
		
        
        }
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        //FIN
        System.out.println("Everyone is already in a text file");
    //}
    }
    }