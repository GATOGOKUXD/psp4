﻿import java.lang.Thread.State;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */



/** * Represents a task that inserts a number of people in a list. * * 
 * @author Eugenia Pérez Martínez */ 

public class AsynchronousTaks { 
    /** * Entry point of the app. * * @param args the command line arguments */ 
    
    public static void main(String[] args) throws InterruptedException { 
        
       
    	int numberOfPeople = 20; 
    	
    	/*
        Thread t1 = new Thread(new PushPeopleToFile (numberOfPeople)); 
        Thread t2 = new Thread(new PushPeopleToList(numberOfPeople)); 
        t1.start();
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.start();}*/
        
    	
    	   
        Thread t1 = new Thread(new PushPeopleToFile (numberOfPeople)); 
        t1.run();
        
        if (t1.getState() != State.RUNNABLE){ 
        	  Thread t2 = new Thread(new PushPeopleToList(numberOfPeople)); 
        	t2.start();}
      
       
       
       //PREGUNTA 1: el programa tarda 10 segundos entrelazando tareas
       
       //PREGUNTA 2: el programa tarda  20  segundos ejecutandose de manera secuencial
    
}}