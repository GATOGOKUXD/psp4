﻿/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */




import java.util.ArrayList; 
import java.util.List; 
import java.util.logging.Level; 
import java.util.logging.Logger; 

/** * Represents a task that inserts a number of people in a list. * 
 * @author Eugenia Pérez Martínez */ 
class PushPeopleToList implements Runnable { 
    
    /** * Number of people to insert. */ 
    private int numberOfPeople;

    /** * Class constructor. * 
     * @param numberOfPeople people to insert. */ 
    public PushPeopleToList(int numberOfPeople) { 
        this.numberOfPeople = numberOfPeople; 
    } 

    @Override public void run() { 
        System.out.println("Starting to push people to an ArrayList");

        //TODO: Crea un ArrayList. A continuación, dentro de un bucle con el 
        //mismo número de iteraciones que personas a insertar, crea una espera 
        //de medio segundo, crea la persona a insertar e insértala 
       
        
        ArrayList pers = new ArrayList();
        
        
        for(int c =0 ;c<numberOfPeople;c++){
        	Persona per = new Persona();
        	System.out.println( (c+1) + "ª  " + per.toString()+ " ARRAYLIST");
        	pers.add(per);
        	
        	try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        
        
        //FIN


        System.out.println("Everyone is already in the ArrayList"); 
    }
}
